=====================
|Proyecto HarambeGag|
=====================

Despues del nefasto incidente en Cincinnati en donde las personas de un zoologico pondero la vida de un niño insensato sobre la de un noble gorilla, un grupo de estudiantes decidio estudiar desde la clandestinidad y crear una pagina web que permita mantener la memoria de un ser vivo como pocos, nuestro salvador, Harambe.

============
Actores
============

Usuario  
Cliente(->Usuario)
Administrador(->Usuario)

============
Casos de Uso
============

CU01: ConsultarPublicacionesPopulares	->	Prot__: Populares
CU02: ConsultarPublicaciones		->	Prot__: Nuevo
CU03: ConsultarPublicacion		->	Prot__: Publicacion
CUExt0301: RegistrarComentario		
CUExt0302: RegistrarReply
CU04: BuscarPublicacion			->	Prot__: Buscar
CU04: ConsultarSeccion			->	Prot_: Seccion
CU03: RegistrarUsuario			->	Prot__: Registro
CUInc01: LoginUsuario			->	Prot__: Login
CU05: RegistrarPublicacion		->	Prot__: Post
CU06: ConsultarPerfil			->	Prot_: Perfil
CU07: MantenerCuentaUsuario		->	Prot_: Preferencias Cuenta
CU08: MantenerContraseñaUsuario		->	Prot_: Preferencias Contraseña
CU09: MantenerPerfilUsuario		->	Prot_: Preferencias Perfil

CU10: MantenerSecciones			->	Prot__: 
CU11: MantenerPublicaciones		->	Prot__:

============
Entidades
============

Usuario
Representa an usuario que puede observar, registrar y comentar las publicaciones. Hay 2 tipos: Cliente y Administrador. Solo un usuario de tipo Administrador puede mantener secciones y publicaciones.

Publicacion
Representa una imagen con un titulo. Puede ser puntuada y comentada por los usuarios.

Seccion
Representa una categoria a la que pertenece una publicacion. Solo puede ser administrada por un usuario de tipo Administrador.

Comentario
Representa una opinion de un usuario sobre una publicacion. Por esto, una publicacion puede tener muchos comentarios inclusive del mismo usuario.