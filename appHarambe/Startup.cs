﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(appHarambe.Startup))]
namespace appHarambe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
