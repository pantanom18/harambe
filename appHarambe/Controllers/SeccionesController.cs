﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using appHarambe.Models;

namespace appHarambe.Controllers
{
    public class SeccionesController : Controller
    {
        private HARAMBE_DBEntities dbcontext = new HARAMBE_DBEntities();

        public ActionResult IndexPartial()
        {
            var consulta = from s in dbcontext.TB_SECCION
                           select s;

            return PartialView(consulta.ToList());
        }
    }
}
